import React from 'react';

import {Grid,Paper,Typography} from '@material-ui/core';

const VideoItem = ({video,onVideoSelect}) =>{
    const videoURL = "https://www.youtube.com/embed/"+video.id.videoId
    return(
        <div style={{paddingTop:"10px"}}>
            <Grid item xs={12}>
                <Paper style={{display:"flex" ,height:"20%"}} onClick={()=>onVideoSelect(video)}>
                    <img style={{marginRight:"10px"}}width="200px" alt="thumnailsss" src={video.snippet.thumbnails.medium.url}/>
                    <Typography variant="subtitle" style={{fontSize:"15px"}}>{video.snippet.title}</Typography>
                </Paper>
            </Grid>
       </div>
    )
}

export default VideoItem;